import React, { Component, Fragment } from 'react';
import TaskListComponent from "./TaskListComponent";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';


class TaskComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tasks: [], text: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  render() {
    const title = 'My Tasks'; 
    return (
      <Container>
        <Row>
        <h3>{title}</h3>
        </Row>
        <Row>
        <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input
            className = "form-control"
            id=""
            placeholder= "Enter task.."
            onChange={this.handleChange}
            value={this.state.text}
          />
          </div>
        </form>
        </Row>
        <Row>
        <TaskListComponent tasks={this.state.tasks} handleDelete={this.handleDelete}/>
        </Row>
      </Container>
    );
  }

  handleDelete(id){
    for( var i = 0; i < this.state.tasks.length; i++){ 
      if ( this.state.tasks[i].id === id) {
        this.state.tasks.splice(i, 1); 
      }
   }
    this.setState(state => ({
      tasks: state.tasks,
      text: ''
    }));

}

  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.text.length) {
      return;
    }
    const newItem = {
      text: this.state.text,
      id: Math.random()
    };
    this.setState(state => ({
      tasks: state.tasks.concat(newItem),
      text: ''
    }));
  }
}

export default TaskComponent;