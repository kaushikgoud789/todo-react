import React, { Component } from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';

class TaskListComponent extends React.Component {
  handleDelete(id){
    this.props.handleDelete(id);
}
  render() {
    return (
      <ListGroup variant="flush">
        {this.props.tasks.map(task => (
          <ListGroup.Item key={task.id}>{task.text} &nbsp; 
          <Button variant="outline-danger" size="sm" onClick={this.handleDelete.bind(this, task.id)}>Delete</Button>
          </ListGroup.Item>
        ))}
      </ListGroup>
    );
  }
}

export default TaskListComponent;