import React from 'react';
import logo from './logo.svg';
import './App.css';
import TaskComponent from './TaskComponent';

function App() {
  return (
    <div className="App">
      <TaskComponent />
    </div>
  );
}

export default App;
